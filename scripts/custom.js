(function($) {
  "use strict";
  var POTENZA = {};

  var $window = $(window),
    $progressBar = $(".skill-bar"),
    $document = $(document);
  $.fn.exists = function() {
    return this.length > 0;
  };
  POTENZA.counters = function() {
    var counter = jQuery(".counter");
    if (counter.length > 0) {
      loadScript(plugin_path + "jquery.countTo.js", function() {
        counter.each(function() {
          var $elem = $(this);
          $elem.find(".timer").countTo();
          // $elem.appear(function () {

          // });
        });
      });
    }
  };

  POTENZA.carousel = function() {
    var owlslider = jQuery("div.owl-carousel");
    if (owlslider.length > 0) {
      loadScript(plugin_path + "owl-carousel.min.js", function() {
        owlslider.each(function() {
          var $this = $(this),
            $items = $this.data("items") ? $this.data("items") : 1,
            $loop = $this.attr("data-loop") ? $this.data("loop") : true,
            $navdots = $this.data("nav-dots") ? $this.data("nav-dots") : false,
            $navarrow = $this.data("nav-arrow")
              ? $this.data("nav-arrow")
              : false,
            $autoplay = $this.attr("data-autoplay")
              ? $this.data("autoplay")
              : true,
            $autospeed = $this.attr("data-autospeed")
              ? $this.data("autospeed")
              : 5000,
            $smartspeed = $this.attr("data-smartspeed")
              ? $this.data("smartspeed")
              : 1000,
            $autohgt = $this.data("autoheight")
              ? $this.data("autoheight")
              : false,
            $space = $this.attr("data-space") ? $this.data("space") : 30;
          $(this).owlCarousel({
            loop: $loop,
            items: $items,
            responsive: {
              0: { items: $this.data("xx-items") ? $this.data("xx-items") : 1 },
              480: {
                items: $this.data("xs-items") ? $this.data("xs-items") : 1
              },
              768: {
                items: $this.data("sm-items") ? $this.data("sm-items") : 2
              },
              980: {
                items: $this.data("md-items") ? $this.data("md-items") : 3
              },
              1200: { items: $items }
            },
            dots: $navdots,
            autoplayTimeout: $autospeed,
            smartSpeed: $smartspeed,
            autoHeight: $autohgt,
            margin: $space,
            nav: $navarrow,
            navText: [
              "<i class='fa fa-angle-left fa-2x'></i>",
              "<i class='fa fa-angle-right fa-2x'></i>"
            ],
            autoplay: $autoplay,
            autoplayHoverPause: true
          });
        });
      });
    }
  };

  POTENZA.progressBar = function() {
    $progressBar = $(".skill-bar");
    if ($progressBar.exists()) {
      $progressBar.each(function(i, elem) {
        var $elem = $(this),
          percent = $elem.attr("data-percent") || "100",
          delay = $elem.attr("data-delay") || "100",
          type = $elem.attr("data-type") || "%";
        if (!$elem.hasClass("progress-animated")) {
          $elem.css({ width: "0%" });
        }
        var progressBarRun = function() {
          $elem
            .animate({ width: percent + "%" }, "easeInOutCirc")
            .addClass("progress-animated");
          $elem
            .delay(delay)
            .append(
              '<span class="progress-type animated fadeIn">' +
                type +
                '</span><span class="progress-number animated fadeIn">' +
                percent +
                "</span>"
            );
        };
        // $(elem).appear(function() {
        //   setTimeout(function() {
        //     progressBarRun();
        //   }, delay);
        // });

        setTimeout(function() {
          progressBarRun();
        }, delay);
      });
    }
  };

  var _arr = {};
  function loadScript(scriptName, callback) {
    if (!_arr[scriptName]) {
      _arr[scriptName] = true;
      var body = document.getElementsByTagName("body")[0];
      var script = document.createElement("script");
      script.type = "text/javascript";
      script.src = scriptName;
      script.onload = callback;
      body.appendChild(script);
    } else if (callback) {
      callback();
    }
  }
  $window.on("load", function() {});
  $document.ready(function() {
    POTENZA.counters();
    POTENZA.carousel();
    POTENZA.progressBar();
  });
})(jQuery);
