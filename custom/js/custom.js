(function ($) {
    "use strict";
    var POTENZA = {};
   
    var $window = $(window),
        $document = $(document),
        $body = $('body'),
        $countdownTimer = $('.countdown'),
        $bar = $('.bar'),
        $pieChart = $('.round-chart'),
        $progressBar = $('.skill-bar'),
        $counter = $('.counter'),
        $datetp = $('.datetimepicker');
    $.fn.exists = function () { return this.length > 0; };
    POTENZA.counters = function ()
    {
        var counter = jQuery(".counter");
        if (counter.length > 0) {
            loadScript(plugin_path + 'jquery.countTo.js', function () {
                counter.each(function () {
                    var $elem = $(this);
                    $elem.find('.timer').countTo();
                    // $elem.appear(function () {
                        
                    // });
                });
            });
        }
    };
    var _arr = {};
    function loadScript(scriptName, callback) {
        if (!_arr[scriptName]) {
            _arr[scriptName] = true;
            var body = document.getElementsByTagName('body')[0];
            var script = document.createElement('script');
            script.type = 'text/javascript';
            script.src = scriptName;
            script.onload = callback;
            body.appendChild(script);
        } else if (callback) { callback(); }
    }; $window.on("load", function () { });
    $document.ready(function () { POTENZA.counters(); });
})(jQuery);